# Maintainer: Antoni Aloy Torrens <aaloytorrens@gmail.com>

# From Arch Linux AUR
# Co-Maintainer: Ckat <ckat@teknik.io>
pkgname=chatterino2
pkgver=2.4.0
pkgrel=0
pkgdesc='Second installment of the Twitch chat client series "Chatterino", release appimage'
arch=('amd64')
url=https://chatterino.com
license=('MIT')
depends=('libqt5multimedia5' 'libqt5svg5' 'libboost-system1.74.0' 'libboost-filesystem1.74.0' 'libboost-random1.74.0' 'qt5-image-formats-plugins' 'openssl')
optdepends=('streamlink: For piping streams to video players'
            'pulseaudio: For audio output'
            'gst-plugins-good: For audio output')

# Do not load strip extension
extensions=("zipman")

source=("https://github.com/Chatterino/chatterino2/releases/download/v$pkgver/Chatterino-x86_64.AppImage")
sha256sums=('67b8b090ac00b90a9391b2264fd45d103ed6e87b252de11dc58ffc9eb2ec1519')

prepare () {
    cd "$srcdir"
    chmod +x Chatterino-x86_64.AppImage
    ./Chatterino-x86_64.AppImage --appimage-extract
}

package() {
    cd "$srcdir"
    install -Dm755 Chatterino-x86_64.AppImage "$pkgdir/opt/chatterino/Chatterino-x86_64.AppImage"
    install -dm755 "$pkgdir/usr/bin"
    ln -s "/opt/chatterino/Chatterino-x86_64.AppImage" "$pkgdir/usr/bin/chatterino"
    install -Dm644 "squashfs-root/com.chatterino.chatterino.desktop" "$pkgdir/usr/share/applications/com.chatterino.chatterino.desktop"
    install -Dm644 "squashfs-root/chatterino.png" "$pkgdir/usr/share/pixmaps/chatterino.png"

    # Symlink pixmaps
    cd "$pkgdir/usr/share/pixmaps/"
    ln -s chatterino.png com.chatterino.chatterino.png
}
